public class Card{
  private int rank;
  private String suit;

  public Card(int rank, String suit){
    this.suit = suit;
    this.rank = rank;
  }

  public int getRank(){
    return this.rank;
  }

  public String getSuit(){
    return this.suit;
  }

  public String toString(){
    String printRank = Integer.toString(this.rank);
    if (this.rank == 1)
      printRank = "A";
    if (this.rank == 11)
      printRank = "J";
    if (this.rank == 12)
      printRank = "Q";
    if (this.rank == 13)
      printRank = "K";

    return printRank + " " + suit;
  }
  public double calculateScore(){
    double score = 0.0;
    score += this.rank;
    if (this.suit.equals("Hearts"))
      score += 0.4;
    if (this.suit.equals("Spades"))
      score += 0.3;
    if (this.suit.equals("Diamonds"))
      score += 0.2;
    if (this.suit.equals("Clubs"))
      score += 0.1;
    return score;
  }
}