import java.util.Scanner;
public class SimpleWar{
  public static void main(String[] args){
    Scanner read = new Scanner(System.in);
    Deck drawPile = new Deck();
    drawPile.shuffle();
    int p1Points = 0;
    int p2Points = 0;
    System.out.println("Press ENTER to start each round");

    while(drawPile.length() > 0){
      System.out.println("Score:\nP1: " + p1Points + "\nP2: " + p2Points);
      read.nextLine();
      Card p1Card = drawPile.drawTopCard();
      Card p2Card = drawPile.drawTopCard();
      System.out.println("P1 card is: " + p1Card + "\nP2 card is: " + p2Card);
      if (p1Card.calculateScore() > p2Card.calculateScore()){
        System.out.println("P1 wins the round\n");
        p1Points++;
      }else{
        System.out.println("P2 wins the round\n");
        p2Points++;
      }
    }

    if(p1Points > p2Points)  
      System.out.println("P1 Wins!");
    if(p1Points < p2Points)
      System.out.println("P2 Wins!");
    else{
      System.out.println("Its a DRAW");
    }
  }
}