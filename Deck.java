import java.util.Random;
public class Deck{
  Card[] cards;
  int numberOfCards;
  Random rng;

  public Deck(){
    this.rng = new Random();
    this.cards = new Card[52];
    String[] suit = {"Hearts", "Spades", "Diamonds", "Clubs"};
    int[] rank = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    int i = 0;
    for(int r : rank){
      for(String s : suit){
        this.cards[i] = new Card(r, s);
        i++;
      }
    }
    this.numberOfCards = this.cards.length;
  }

  public int length(){
    return numberOfCards;
  }

  public Card drawTopCard(){
    this.numberOfCards--;
    return this.cards[numberOfCards];
  } 

  public void shuffle(){
    for (int i = 0; i < this.numberOfCards; i++){
      int x = this.rng.nextInt(this.numberOfCards-i)+i;
      Card c = this.cards[i];
      this.cards[i] = this.cards[x];
      this.cards[x] = c;
    }
  } 

  public String toString(){
    String s = "your deck is " + this.numberOfCards + " cards, here are the cards: \n";
    for(int i = 0; i < this.numberOfCards; i++){
      s += cards[i].toString() + "\n";
    }
    return s;
  }
}